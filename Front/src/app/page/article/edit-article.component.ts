import { ArticleService } from './article.service';
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector : 'app-edit-article',
    templateUrl : './edit-article.compent.html',
    styleUrls : ['./edit-article.component.scss'],
})

export class EditArticleComponent{
    userId : string;
    article: { id?: string, titre? : string, contenu?: string } = { }
    constructor(
        private articleService : ArticleService,
        private router : Router,
    ){
        
        if(sessionStorage.getItem('userSession')){
            this.userId = JSON.parse(sessionStorage.getItem('userSession')).id;
            console.log(" SESSION Id " + this.userId);
        }
        else{
            this.router.navigateByUrl('/login');
        }
        

    }
    
    async loadArticle(){
        if(this.article){
            this.article = await this.articleService.getArticle(this.article.id);
        }
    }

    async addArticle(){
        if(this.article){
            await this.articleService.addArticle(this.article).then(x => {
                console.log("Ajouté avec succès");
                this.router.navigateByUrl("/article");
            });
            
        }
    }


}