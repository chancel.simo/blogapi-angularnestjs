import { ArticleService } from './article.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  articles : { titre? : string, contenu?: string }[] = [];
  headers : string[] = ['titre','contenu', 'action'];
  constructor(
    private router : ActivatedRoute,
    private route : Router,
    private articleService : ArticleService
  ) {

    if(!sessionStorage.getItem('userSession')){
      this.route.navigate(['/login'])
    }

  }

  async ngOnInit(){
     this.articles = await this.articleService.getArticles();
    // if(!this.userDto)
    //   this.userDto = { }
     console.log(this.articles);
    
  }

}
