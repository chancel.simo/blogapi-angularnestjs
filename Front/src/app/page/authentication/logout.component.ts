import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: 'app-logout',
  template : 'Logout...'
})
export class LogoutComponent{

    constructor(
    private router: Router
    ){
        if(sessionStorage.getItem('userSession'))
        {
        sessionStorage.clear();
        this.router.navigate(['/login'], { queryParams : { logout : 1 } })
        }
    }
}