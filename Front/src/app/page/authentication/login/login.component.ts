import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService, userDto } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userDto : userDto = { };
  failedMessage: string;

  constructor(
    private loginService : LoginService,
    private router : Router,
    private route: ActivatedRoute,
  ) { 
    

  }

  async ngOnInit(){
    // this.userDto = await this.loginService.getUser();
    // if(!this.userDto)
    //   this.userDto = { }
    // console.log(this.userDto);
    
  }

  async auth(){
    if(!this.userDto.login || !this.userDto.password){
      this.failedMessage = 'Veuillez remplir correctement tout les champs';
    }
    else{
      this.failedMessage = null;
    }
    
    if(!this.failedMessage){
      await this.loginService.login({ login : this.userDto.login, password : this.userDto.password }).then(
        x => {
          console.log(x)
          if(sessionStorage.getItem('userSession')){
            this.router.navigate(['/article']);
          }
        }
      );
    }
    
  }

  logFormData(){
    // this.loginService.addUser(this.userDto);
  }

}
