import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs";

export interface userDto{
    login?:string;
    email?:string;
    lastName?:string; 
    password?:string; 

    tel?:string;
}
export class getAuthRequest{
    id?:string;
    login?:string;
    password?:string;
}

@Injectable({
    providedIn : 'root',
})
export class LoginService{

    userConnected: userDto;

    url = "http://localhost:3000/users/";
    constructor(
        private http: HttpClient,
        private route: Router,
    ){}

    async getUsers(){
        let data : any;
        await this.http.get(this.url).toPromise().then( r => {
            data = r
        });
        return data;
    }

    async getUser(id: string = undefined ){
        let data : any;
        await this.http.get(`${this.url + id}`).toPromise().then( r => {
            data = r
        });
        return data;
    }

    async addUser(userDto: userDto){
        return await this.http.post(this.url, userDto).subscribe(
            () => {
              console.log('Enregistrement terminé !');
            },
            (error) => {
              console.log('Erreur ! : ' + error);
            }
          );
    }
    async login(user: getAuthRequest){
        console.log(user)

        return await this.http.post(this.url+'auth/login/', user).subscribe(
            (data) => {
                console.log(data)
                sessionStorage.setItem('userSession', JSON.stringify(data))
                this.userConnected = JSON.parse(sessionStorage.getItem('userSession'))
                console.log(this.userConnected);
                this.route.navigate(['/article']);

            },
            (error:HttpErrorResponse) => {
                console.log('Erreur ! : ' + error.message);
            }
        )
    }
}

