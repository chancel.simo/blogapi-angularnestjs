import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { userDto } from './page/authentication/login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  user:userDto = JSON.parse(sessionStorage.getItem('userSession'));
  title = 'Front';
  constructor(
    private router: Router,
    private route: ActivatedRoute,

  ){
    console.log(this.user);
  }

  logout(){
    this.router.navigateByUrl('/logout');
  }
}
