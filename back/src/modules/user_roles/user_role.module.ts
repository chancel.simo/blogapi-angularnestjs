import { UserRoleService } from './user_role.service';
import { UserRoleEntity } from './user_role.entity';
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
    imports: [TypeOrmModule.forFeature([UserRoleEntity])],
    providers: [UserRoleService],
    exports : [UserRoleService]
})
export class UsersRoleModule{
    
 }