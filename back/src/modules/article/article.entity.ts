
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from "typeorm";
import { BaseEntity } from "../baseFiles/baseEntity";
import { UserEntity } from "../user/user.entity";

@Entity('article')

export class Article extends BaseEntity{
    @Column({type: "varchar", length:20, nullable:true})
    titre:string;

    @Column({type: "varchar", length:20, nullable:true})
    contenu:string;

    @ManyToOne(() => UserEntity, user => user.articles)
    user: UserEntity;

}