import { ApiPropertyOptional } from "@nestjs/swagger";

export class ArticleDto{
    @ApiPropertyOptional()
    titre:string;

    @ApiPropertyOptional()
    content:string;
}