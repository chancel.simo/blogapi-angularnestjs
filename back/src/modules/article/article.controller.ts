
import { Controller, Get, Post, Body } from "@nestjs/common";
import { ArticleService } from "./article.service";

@Controller('/article')

export class ArticleController{
    constructor(
        private articleService: ArticleService
    ){
    }

    @Get()
    async getArticles(){
        return await this.articleService.getArticles()
    }

    @Post()
    async addArticle(@Body() articleDto: { titre:string, contenu:string , userId:string} ){
        console.log(articleDto);
        return await this.articleService.addArticle(articleDto);
    }
    
}