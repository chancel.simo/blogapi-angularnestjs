import { Article } from './../article/article.entity';

import { Column, Entity, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { BaseEntity } from "../baseFiles/baseEntity";
import { UserRoleEntity } from "../user_roles/user_role.entity";

@Entity('user')

export class UserEntity extends BaseEntity{
    @Column({type: "varchar", length:20, nullable:false})
    login:string;

    @Column({type: "varchar", length:20, nullable:true})
    email:string;

    @Column({type:"varchar"})
    password:string;

    @Column({type:"varchar", length:50, nullable:true})
    lastName:string;

    @Column({type:"varchar", length:50, nullable:true})
    tel:string;

    @OneToOne(() => UserRoleEntity, { cascade : true })
    @JoinColumn()
    role:UserRoleEntity;

    @OneToMany(() => Article, article => article.user)
    articles: Article[];
}