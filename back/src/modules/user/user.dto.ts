import { userRoleDto } from './../user_roles/user_role.dto';
import { ApiPropertyOptional } from "@nestjs/swagger";

export class UserDto{
    @ApiPropertyOptional()
    login:string;
    @ApiPropertyOptional()
    email?:string;
    @ApiPropertyOptional()
    password:string;

    @ApiPropertyOptional()
    tel?:string;

    @ApiPropertyOptional()
    lastName?:string;

    @ApiPropertyOptional({ type : ()=> userRoleDto })
    role?:userRoleDto;
}

export class getAuthRequest{
    @ApiPropertyOptional()
    id:string;
    @ApiPropertyOptional()
    login?:string;
    @ApiPropertyOptional()
    password?:string;
}

export class GetUserRequest{
    @ApiPropertyOptional()
    login:string;
    @ApiPropertyOptional()
    email?:string;
    @ApiPropertyOptional()
    password:string;
    @ApiPropertyOptional()
    lastName?:string;
    @ApiPropertyOptional()
    tel?:string;
    
    @ApiPropertyOptional({ type : ()=> userRoleDto })
    role?:userRoleDto;
}

export class GetUserResponse{
    @ApiPropertyOptional()
    login:string;
    @ApiPropertyOptional()
    email?:string;
    @ApiPropertyOptional()
    lastName?:string;
    @ApiPropertyOptional()
    password?:string;
    
    @ApiPropertyOptional()
    tel?:string;

    @ApiPropertyOptional({ type : ()=> userRoleDto })
    role?:userRoleDto;
}

export class GetUsersResponse{
    @ApiPropertyOptional({isArray: true})
    userDto: UserDto[];
}